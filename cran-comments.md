## Test environments
* local Ubuntu 20.04 install, R 3.6.3
* win-builder (devel)
* r-hub (Ubuntu+Fedora Linux)

## R CMD check results
There were no ERRORs, WARNINGs or NOTEs (except the new release / archived NOTE)

## Downstream dependencies
I have also run R CMD check on downstream dependencies of tidytidbits.
All packages that I could install passed.
